#include <ESP32Servo.h>

#define led 3
Servo servo1, servo2, servo3, servo4, servo5;
int value;
int butt = 26;
double angle;

void setup() {
  servo1.attach(18);
  servo2.attach(19);
  servo3.attach(23);
  servo4.attach(22);
  servo5.attach(21);
  pinMode(butt, INPUT);
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);
  value = digitalRead(butt);
  if(value == HIGH) {
    angle = 90;
  } else {
    angle = 0;
  }
  servo1.write(angle);
  servo2.write(angle);
  servo3.write(angle);
  servo4.write(angle);
  servo5.write(angle);
  delay(100);
}
